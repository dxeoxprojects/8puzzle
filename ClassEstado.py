import random


class Estado(object):
    def __init__(self):
        #self.matriz = [[0, 0, 0], [0, 0, 0], [0, 0, 0]]
        #Prueba
        self.matriz = [[4, 2, 3], [1, 8, 6], [7, 5, 9]]
        self.matrizResultado = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]

    def Verificar(self, num):
        for i in range(3):
            for j in range(3):
                if num == self.matriz[i][j]:
                    return True
        return False

    def LLenarMatriz(self):
        for i in range(3):
            for j in range(3):
                num = random.randint(0, 9)
                while self.Verificar(num):
                    num = random.randint(0, 9)
                self.matriz[i][j] = num

    def ImprimirMatriz(self):
        for i in range(3):
            for j in range(3):
                if self.matriz[i][j] == 9:
                    print('-\t', end='')
                else:
                    print(self.matriz[i][j], end='\t')
            print('')

    def getInvCount(self):
        inv_count = 0
        arreglo=[]
        for i in range(3):
            for j in range(3):
                arreglo.append(self.matriz[i][j])
        for i in range(9):
            for j in range(i, 9):
                if arreglo[j] < arreglo[i] < 9:
                    inv_count += 1
        return inv_count

    def isSolvable(self):
        invCount = self.getInvCount()
        if invCount % 2 == 0:
            print("Tiene solución")
            return True
        else:
            print("No tiene solución")
            return False

    #Distancia Manhattan Heuristica

    def Distancia(self, matriz1):
        pasos=0
        for i in range(3):
            for j in range(3):
                for h in range(3):
                    for k in range(3):
                        if matriz1[i][j] == self.matrizResultado[h][k] and matriz1[i][j] != 9:
                            pasos = pasos + abs(i-h) + abs(j-k)
        #print('Heuristica: '+ str(pasos))
        return pasos