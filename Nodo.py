from ClassEstado import Estado
import sys

sys.setrecursionlimit(10 ** 9)
Lista = []
ListaExpandidos = []

class Nodo(object):

    def __init__(self, nivel):
        self.Abajo = None
        self.Arriba = None
        self.Derecha = None
        self.Izquierda = None
        self.Padre = None
        self.Nivel = nivel
        self.e = Estado()

    # Hijo0= Abajo
    # Hijo1= Arriba
    # Hijo2= Derecha
    # Hijo3= Izquierda

    def IntercambioPosiciones(self, cod, i, j, nodo):
        matrizProvisional = [[0, 0, 0], [0, 0, 0], [0, 0, 0]]
        for f in range(3):
            for c in range(3):
                matrizProvisional[f][c] = nodo.e.matriz[f][c]
        if cod == 0:
            #print("\nMovimiento para abajo: ")
            aux = matrizProvisional[i][j]
            matrizProvisional[i][j] = matrizProvisional[i - 1][j]
            matrizProvisional[i - 1][j] = aux
        elif cod == 1:
            #print("\nMovimiento para arriba:")
            aux = matrizProvisional[i][j]
            matrizProvisional[i][j] = matrizProvisional[i + 1][j]
            matrizProvisional[i + 1][j] = aux
        elif cod == 2:
            #print("\nMovimiento para la derecha:")
            aux = matrizProvisional[i][j]
            matrizProvisional[i][j] = matrizProvisional[i][j - 1]
            matrizProvisional[i][j - 1] = aux
        else:
            #print("\nMovimiento para la izquierda:")
            aux = matrizProvisional[i][j]
            matrizProvisional[i][j] = matrizProvisional[i][j + 1]
            matrizProvisional[i][j + 1] = aux

        ''''for i in range(3):
            for j in range(3):
                if matrizProvisional[i][j] == 9:
                    print("-\t", end='')
                else:
                    print(matrizProvisional[i][j], end='\t')
            print()'''
        return matrizProvisional

    def Sucesores(self, nodo, FlagAEstrella, numNodos):
        ListaExpandidos.append(nodo.e.matriz)
        if FlagAEstrella:
            Costo = nodo.Nivel + 1
        else:
            Costo=0
        '''print('\nMatriz Padre')
        for i in range(3):
            for j in range(3):
                if nodo.e.matriz[i][j] == 9:
                    print("-\t", end='')
                else:
                    print(nodo.e.matriz[i][j], end='\t')
            print()
        print('\n----------Nivel: '+str(Costo)+'------------')'''
        for i in range(3):
            for j in range(3):
                if 9 == nodo.e.matriz[i][j]:
                    if i != 0:
                        n = 0
                        matrizp = self.IntercambioPosiciones(n, i, j, nodo)
                        if self.e.matrizResultado == matrizp:
                            Hijo0 = Nodo(Costo)
                            nodo.Abajo = Hijo0
                            Hijo0.Padre = nodo
                            Hijo0.e.matriz = matrizp
                            self.NodoInicio(Hijo0)
                            print('Numero total de nodos:  ' + str(numNodos))
                            return True
                        if self.VerificarInicio(matrizp) != True:
                            if nodo.Padre is not None:
                                NodoAux = nodo.Padre
                                if NodoAux.e.matriz!=matrizp:
                                    Hijo0 = Nodo(Costo)
                                    numNodos = numNodos + 1
                                    nodo.Abajo = Hijo0
                                    Hijo0.e.matriz = matrizp
                                    Hijo0.Padre = nodo
                                    Lista.append([Hijo0, self.e.Distancia(matrizp) + Costo])
                            else:
                                Hijo0 = Nodo(Costo)
                                numNodos = numNodos + 1
                                nodo.Abajo = Hijo0
                                Hijo0.e.matriz = matrizp
                                Hijo0.Padre = nodo
                                Lista.append([Hijo0, self.e.Distancia(matrizp) + Costo])
                        if j != 0:
                            n = 2
                            matrizp = self.IntercambioPosiciones(n, i, j, nodo)
                            if self.e.matrizResultado == matrizp:
                                Hijo2 = Nodo(Costo)
                                nodo.Derecha = Hijo2
                                Hijo2.Padre = nodo
                                Hijo2.e.matriz = matrizp
                                self.NodoInicio(Hijo2)
                                numNodos = numNodos + 1
                                print('Numero total de nodos:  ' + str(numNodos))
                                return True
                            if self.VerificarInicio(matrizp) != True:
                                if nodo.Padre is not None:
                                    NodoAux = nodo.Padre
                                    if NodoAux.e.matriz != matrizp:
                                        Hijo2 = Nodo(Costo)
                                        numNodos = numNodos + 1
                                        nodo.Derecha = Hijo2
                                        Hijo2.e.matriz = matrizp
                                        Hijo2.Padre = nodo
                                        Lista.append([Hijo2, self.e.Distancia(matrizp) + Costo])
                                else:
                                    Hijo2 = Nodo(Costo)
                                    numNodos = numNodos + 1
                                    nodo.Derecha = Hijo2
                                    Hijo2.e.matriz = matrizp
                                    Hijo2.Padre = nodo
                                    Lista.append([Hijo2, self.e.Distancia(matrizp) + Costo])
                        if j != 2:
                            n = 3
                            matrizp = self.IntercambioPosiciones(n, i, j, nodo)
                            if self.e.matrizResultado == matrizp:
                                Hijo3 = Nodo(Costo)
                                nodo.Izquierda = Hijo3
                                Hijo3.Padre = nodo
                                Hijo3.e.matriz = matrizp
                                self.NodoInicio(Hijo3)
                                numNodos = numNodos + 1
                                print('Numero total de nodos:  ' + str(numNodos))
                                return True
                            if self.VerificarInicio(matrizp) != True:
                                if nodo.Padre is not None:
                                    NodoAux = nodo.Padre
                                    if NodoAux.e.matriz != matrizp:
                                        Hijo3 = Nodo(Costo)
                                        numNodos = numNodos + 1
                                        nodo.Izquierda = Hijo3
                                        Hijo3.e.matriz = matrizp
                                        Hijo3.Padre = nodo
                                        Lista.append([Hijo3, self.e.Distancia(matrizp) + Costo])
                                else:
                                    Hijo3 = Nodo(Costo)
                                    numNodos = numNodos + 1
                                    nodo.Izquierda = Hijo3
                                    Hijo3.e.matriz = matrizp
                                    Hijo3.Padre = nodo
                                    Lista.append([Hijo3, self.e.Distancia(matrizp) + Costo])

                    if i != 2:
                        n = 1
                        matrizp = self.IntercambioPosiciones(n, i, j, nodo)
                        if self.e.matrizResultado == matrizp:
                            Hijo1 = Nodo(Costo)
                            nodo.Arriba = Hijo1
                            Hijo1.Padre = nodo
                            Hijo1.e.matriz=matrizp
                            self.NodoInicio(Hijo1)
                            numNodos = numNodos + 1
                            print('Numero total de nodos:  ' + str(numNodos))
                            return True
                        if self.VerificarInicio(matrizp) != True:
                            if nodo.Padre is not None:
                                NodoAux = nodo.Padre
                                if NodoAux.e.matriz != matrizp:
                                    Hijo1 = Nodo(Costo)
                                    numNodos = numNodos + 1
                                    nodo.Arriba = Hijo1
                                    Hijo1.e.matriz = matrizp
                                    Hijo1.Padre = nodo
                                    Lista.append([Hijo1, self.e.Distancia(matrizp) + Costo])
                            else:
                                Hijo1 = Nodo(Costo)
                                numNodos = numNodos + 1
                                nodo.Arriba = Hijo1
                                Hijo1.e.matriz = matrizp
                                Hijo1.Padre = nodo
                                Lista.append([Hijo1, self.e.Distancia(matrizp) + Costo])

                        if i == 0:
                            if j != 0 & i == 0:
                                n = 2
                                matrizp = self.IntercambioPosiciones(n, i, j, nodo)
                                if self.e.matrizResultado == matrizp:
                                    Hijo2 = Nodo(Costo)
                                    nodo.Derecha = Hijo2
                                    Hijo2.Padre = nodo
                                    Hijo2.e.matriz = matrizp
                                    self.NodoInicio(Hijo2)
                                    numNodos = numNodos + 1
                                    print('Numero total de nodos:  ' + str(numNodos))
                                    return True
                                if self.VerificarInicio(matrizp) != True:
                                    if nodo.Padre is not None:
                                        NodoAux = nodo.Padre
                                        if NodoAux.e.matriz != matrizp:
                                            Hijo2 = Nodo(Costo)
                                            numNodos = numNodos + 1
                                            nodo.Derecha = Hijo2
                                            Hijo2.e.matriz = matrizp
                                            Hijo2.Padre = nodo
                                            Lista.append([Hijo2, self.e.Distancia(matrizp) + Costo])
                                    else:
                                        Hijo2 = Nodo(Costo)
                                        numNodos = numNodos + 1
                                        nodo.Derecha = Hijo2
                                        Hijo2.e.matriz = matrizp
                                        Hijo2.Padre = nodo
                                        Lista.append([Hijo2, self.e.Distancia(matrizp) + Costo])

                            if j != 2:
                                n = 3
                                matrizp = self.IntercambioPosiciones(n, i, j, nodo)
                                if self.e.matrizResultado == matrizp:
                                    Hijo3 = Nodo(Costo)
                                    nodo.Izquierda = Hijo3
                                    Hijo3.Padre = nodo
                                    Hijo3.e.matriz = matrizp
                                    self.NodoInicio(Hijo3)
                                    numNodos = numNodos + 1
                                    print('Numero total de nodos:  '+ str(numNodos))
                                    return True
                                if self.VerificarInicio(matrizp) != True:
                                    if nodo.Padre is not None:
                                        NodoAux = nodo.Padre
                                        if NodoAux.e.matriz != matrizp:
                                            Hijo3 = Nodo(Costo)
                                            numNodos = numNodos + 1
                                            nodo.Izquierda = Hijo3
                                            Hijo3.e.matriz = matrizp
                                            Hijo3.Padre = nodo
                                            Lista.append([Hijo3, self.e.Distancia(matrizp) + Costo])
                                    else:
                                        Hijo3 = Nodo(Costo)
                                        numNodos = numNodos + 1
                                        nodo.Izquierda = Hijo3
                                        Hijo3.e.matriz = matrizp
                                        Hijo3.Padre = nodo
                                        Lista.append([Hijo3, self.e.Distancia(matrizp) + Costo])
        #print('--------------------------------------------')
        if FlagAEstrella == True:
            self.Sucesores(self.SeleccionarNodo(), True, numNodos)
        else:
            self.Sucesores(self.SeleccionarNodo(), False, numNodos)


    def SeleccionarNodo(self):
        ubi=0
        CostoMenor = Lista[ubi]
        for i in range(len(Lista)):
            if CostoMenor[1] > Lista[i][1]:
                CostoMenor = Lista[i]
                ubi=i
        Lista.pop(ubi)
        return CostoMenor[0]

    def NodoInicio(self, nodo):
        NodosSucesores = []
        while nodo.Padre is not None:
            AuxNodo = nodo.Padre
            NodosSucesores.append(nodo)
            nodo = AuxNodo
        self.ImprimirCostoCamino(nodo, NodosSucesores)

    def ImprimirCostoCamino(self, nodo, NodosSucesores):
        print("--------------- 8 Puzzle --------------")
        for cont in range(len(NodosSucesores)+1):
            for i in range(3):
                for j in range(3):
                    if nodo.e.matriz[i][j] == 9:
                        print("-\t", end='')
                    else:
                        print(nodo.e.matriz[i][j], end='\t')
                print('')
            nodo = NodosSucesores[len(NodosSucesores)-cont-1]
            print('--------------------------------------')
            if(cont !=len(NodosSucesores) ):
                print('Movimiento'+ str(cont+1))

    def LimpiarLista(self):
        Lista.clear()
        ListaExpandidos.clear()
        
    def LimpiarNodo(self, nodo):
        nodo.Abajo = None
        nodo.Arriba = None
        nodo.Derecha = None
        nodo.Izquierda = None
        nodo.Padre = None
        nodo.Nivel = 0

    def VerificarInicio(self, matriz):
        for k in range (len(ListaExpandidos)):
            if self.CompararMatriz(matriz, k):
                return True
        return False

    def CompararMatriz(self, matriz, n):
        for i in range(3):
            for j in range(3):
                if matriz[i][j] != ListaExpandidos[n][i][j]:
                    return False
        return True