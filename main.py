import Nodo


print("8 Puzzle \t")
p = Nodo.Nodo(0)
#p.e.LLenarMatriz()
p.e.ImprimirMatriz()
if p.e.isSolvable():
    p.e.Distancia(p.e.matriz)
    #Costo camino mas corto
    p.Sucesores(p, True, 1)
    print("\n\n**********+*ALGORITMO*********************")
    print("***********PRIMERO MAS VORAZ**************")
    #Primero mas voraz
    p.LimpiarLista()
    p.LimpiarNodo(p)
    p.Sucesores(p, False, 1)
